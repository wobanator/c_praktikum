#ifndef __VECTOR_H__
#define __VECTOR_H__

struct __Vektor__
{
   double x;
   double y;
   double z;
};
typedef struct __Vektor__ Vektor;

extern Vektor vektor_lese_ein(const char * format, ...);
extern void vektor_gib_aus(const Vektor v, const char * name);
extern void vektor_gib_aus_kurz(const Vektor v);

extern Vektor vektor_erstelle(double x, double y, double z);

extern int vektor_vegleiche(const Vektor a, const Vektor b);
extern Vektor vektor_addiere(const Vektor a, const Vektor b);
extern Vektor vektor_multipliziere(const Vektor v, double skalar);
extern double vektor_laenge(const Vektor v);


#endif /* __VECTOR_H__ */
