#include "vektor.h"
#include <stdarg.h>
#include <stdio.h>
#include <math.h>

Vektor vektor_lese_ein(const char * format, ...)
{
   va_list va_args;

   va_start(va_args, format);
   vprintf(format, va_args);
   va_end(va_args);

   Vektor v;

   const char * vektor_style = "(x,y,z): ";

   puts(vektor_style);
   do {
      int values_read = scanf("%lf,%lf,%lf", &v.x, &v.y, &v.z);

      if (values_read == 3) break;

      // clear buffer
      while (getchar() != '\n') ; // empty body

      printf ("Es konnten nur %0d Werte gelesen werden!!!\n"
              "bitte erneute Eingabe %s", values_read, vektor_style);
   } while (1);

   return v;
}

void vektor_gib_aus(const Vektor v, const char * name )
{
   printf("Vektor '%s': ", name);
   vektor_gib_aus_kurz(v);
}

void vektor_gib_aus_kurz(const Vektor v)
{
   printf("(%g, %g, %g)", v.x, v.y, v.z);
}

Vektor vektor_erstelle(double x, double y, double z)
{
   Vektor v = { x, y, z };
   return v;
}

int vektor_vegleiche(const Vektor a, const Vektor b)
{
   return 
      fabs(a.x - b.x) < 0.0001 &&
      fabs(a.y - b.y) < 0.0001 &&
      fabs(a.z - b.z) < 0.0001;
}

Vektor vektor_addiere(const Vektor a, const Vektor b)
{
   Vektor ret;
   ret.x = a.x + b.x;
   ret.y = a.y + b.y;
   ret.z = a.z + b.z;
   return ret;
}

Vektor vektor_multipliziere(const Vektor v, double skalar)
{
   Vektor ret;
   ret.x = v.x * skalar;
   ret.y = v.y * skalar;
   ret.z = v.z * skalar;
   return ret;
}

double vektor_laenge(const Vektor v)
{
   return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}
