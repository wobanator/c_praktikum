#include "vektor_test.h"
#include "vektor.h"
#include <math.h>
#include <stdio.h>

int vektor_test_einlese()
{
   Vektor v = vektor_lese_ein("gib den vektor (1/2/3) ein. ");

   printf("Es wurde: "); vektor_gib_aus(v, "v"); printf(" eingelesen\n");

   return vektor_vegleiche(v, vektor_erstelle(1,2,3));
}

int vektor_test_add()
{
   Vektor a = vektor_erstelle(1, 2, 3);
   Vektor b = vektor_erstelle(4, 5, 6);

   Vektor ergeb = vektor_addiere(a, b);

   Vektor null_vektor = vektor_addiere(ergeb, vektor_erstelle(-5, -7, -9));

   // nur wegen integer werden auf komplette gleichheit überprüfbar
   // sonst bei double werten auf ein delta prüfen, wegen cpu-rundungsfehler
   return fabs(vektor_laenge(null_vektor)) < 0.001;
}

int vektor_test_mult()
{
   Vektor a = vektor_erstelle(1, 2, 3);

   Vektor vek_skaliert = vektor_multipliziere(a, 2);
   return fabs(vektor_laenge(a) * 2 - vektor_laenge(vek_skaliert)) < 0.001;
}
