#include <stdio.h>
#include "vektor_test.h"

int main(void)
{
   if (!vektor_test_einlese()) {
      fprintf(stderr, "Eingabe test fur Vektor-Structur fehlgeschlagen\n");
      return 1;
   }
   if (!vektor_test_add()) {
      fprintf(stderr, "Additions test fur Vektor-Structur fehlgeschlagen\n");
      return 1;
   }
   if (!vektor_test_mult()) {
      fprintf(stderr, "Multiplikations test fur Vektor-Structur fehlgeschlagen\n");
      return 1;
   }
   printf("Alle Tests erfolgreich\n");
   return 0;
}
