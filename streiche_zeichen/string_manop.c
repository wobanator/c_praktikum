#include "string_manop.h"
#include <string.h>

void string_loesche_zeichen_0(char * string, char zeichen)
{
    char * found_char = string;
    while ((found_char = strchr(found_char, zeichen)) != NULL)
    {
        char * string_after = found_char + 1;        
        int bytes_to_copy = strlen(string_after) + 1; // + 1 equals \0
        // have to use memmove because both memories overlap
        memmove(found_char, string_after, bytes_to_copy);
    }
}
