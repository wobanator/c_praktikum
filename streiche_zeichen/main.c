#include "string_manop.h"
#include <string.h>
#include <stdio.h>

#define SIZE 1000

int main(void)
{
    printf("gib string ein: ");

    char string[SIZE];
    fgets(string, SIZE, stdin);
    string[strlen(string)-1] = '\0';

    printf("nun wird das zeichen 'x' heraus geloescht\n");

    string_loesche_zeichen_0(string, 'x');
    
    printf("manipulierter string: '%s'\n", string);
    return 0;
}
