#include "io.h"
#include <stdio.h>

static void test_io_read_string();
static void test_io_read_signed_number();
static void test_io_read_unsigned_number();

int main(void)
{
    test_io_read_string();
    test_io_read_signed_number();
    test_io_read_unsigned_number();
    return 0;
}

void test_io_read_string()
{
	printf("============= %s ===============\n", __func__);
	
    char buffer[100];

    io_read_string(buffer, 100, "gib string ein: ");
    printf("eingelesener string: ``%s''\n", buffer);

    io_read_string(buffer, 10, "gib langen string ein (mindestens %0d): ", 10);
    printf("eingelesener string: ``%s''\n", buffer);
}

void test_io_read_signed_number()
{
	printf("============= %s ===============\n", __func__);
	
    int number = io_read_unsigned_number("gib zahl ein (z.B. %d): ", 100);
    printf("eingelesene Zahl: %0d\n", number);

    printf("noch eine Zahl: ");
    number = io_read_unsigned_number(NULL);
    printf("eingelesene Zahl: %0d\n", number);
}

void test_io_read_unsigned_number()
{
	printf("============= %s ===============\n", __func__);

    unsigned int number = io_read_unsigned_number(
        "gib vorzeichenlose zahl ein (z.B. %d): ", 100);
    printf("eingelesene Zahl: %0u\n", number);

    printf("noch eine Zahl: ");
    number = io_read_signed_number(NULL);
    printf("eingelesene Zahl: %0u\n", number);
}
