#include "saeure.h"
#include <stdio.h>
#include <stdlib.h>

Saeure saeure_get_first() { return kohle; }
Saeure saeure_get_last()  { return kali + 1; }
Saeure saeure_get_next(Saeure s) { return s + 1; }

const char * saeure_get_string(Saeure s)
{
    switch (s) {
        case kohle: return "kohle";
        case salz:  return "salz";
        case kali:  return "kali";
        default:
            fprintf(stderr, "fatal error: saeure_get_string: %0d\n", s);
            exit(1);
            return "";
    }
}
