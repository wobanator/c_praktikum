#pragma once

typedef enum { kohle, salz, kali } Saeure;

extern Saeure saeure_get_first();
extern Saeure saeure_get_last();
extern Saeure saeure_get_next(Saeure s);

extern const char * saeure_get_string(Saeure s);
