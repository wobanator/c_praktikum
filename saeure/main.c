
#include <stdio.h>
#include "saeure.h"

int main(void)
{
    printf(" %15s ", "");
    for (int s = saeure_get_first(); s != saeure_get_last(); s = saeure_get_next(s)) {
        printf("| %s15 ", saeure_get_string(s));
    }
    printf("\n");
    return 0;
}
