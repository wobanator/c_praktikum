#include "lookup.h"
#include <stdio.h>
#include <math.h>

int main(void)
{
    printf("sqrt(2.0) => %f (from math: %f)\n", lookup_sqrt(2.0), sqrt(2.0));
    printf("sqrt(2.5) => %f (from math: %f)\n", lookup_sqrt(2.5), sqrt(2.5));
    printf("sqrt(3.0) => %f (from math: %f)\n", lookup_sqrt(3.0), sqrt(3.0));

    printf("sqrt(2.0) * sqrt(1.5) => %f (from math: %f)\n", lookup_sqrt_prod(2.0, 1.5), sqrt(2.0) * sqrt(1.5));
    printf("sqrt(2.5) * sqrt(0.5) => %f (from math: %f)\n", lookup_sqrt_prod(2.5, 0.5), sqrt(2.5) * sqrt(0.5));
    printf("sqrt(3.0) * sqrt(1.0) => %f (from math: %f)\n", lookup_sqrt_prod(3.0, 1.0), sqrt(3.0) * sqrt(1.0));
    return 0;
}
