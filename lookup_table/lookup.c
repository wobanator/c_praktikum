#include "lookup.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

static const double s_table_sqrt[] = {
    /* 0.0 */ 0.000, /* 0.5 */ 0.707, /* 1.0 */ 1.000, /* 1.5 */ 1.224,
    /* 2.0 */ 1.414, /* 2.5 */ 1.581, /* 3.0 */ 1.732, /* 3.5 */ 1.870
};
static const double s_table_sqrt_size = sizeof(s_table_sqrt) / sizeof(*s_table_sqrt);

static int get_num_idx(double num)
{
    int idx;
    double cmp;
    for (cmp = 0, idx = 0; idx < s_table_sqrt_size; idx++, cmp += 0.5) {
        if (fabs(num - cmp) < 0.01) return idx;
    }
    // number not in range
    fprintf(stderr, "cannot get sqrt-index for %f\n", num);
    exit(1);
    return -1;
}

double lookup_sqrt(double num)
{
    int idx = get_num_idx(num); 
    // idx is always valid for s_table_sqrt
    return s_table_sqrt[idx];
}

static const double s_table_prod_with_0_0[] = { 0, 0, 0, 0, 0, 0, 0, 0 };
static const double s_table_prod_with_0_5[] = { 
    /* 0.0 */ 0.0000, /* 0.5 */ 0.5000, /* 1.0 */ 0.7071, /* 1.5 */ 0.8660,
    /* 2.0 */ 1.0000, /* 2.5 */ 1.1180, /* 3.0 */ 1.2247, /* 3.5 */ 1.3229 };
static const double s_table_prod_with_1_5[] = { 
    /* 0.0 */ 0.0000, /* 0.5 */ 0.8660, /* 1.0 */ 1.2247, /* 1.5 */ 1.5000,
    /* 2.0 */ 1.7321, /* 2.5 */ 1.9365, /* 3.0 */ 2.1213, /* 3.5 */ 2.2913 };
static const double s_table_prod_with_2_0[] = { 
    /* 0.0 */ 0.0000, /* 0.5 */ 1.0000, /* 1.0 */ 1.4142, /* 1.5 */ 1.7321,
    /* 2.0 */ 2.0000, /* 2.5 */ 2.2361, /* 3.0 */ 2.4495, /* 3.5 */ 2.6458 };
static const double s_table_prod_with_2_5[] = { 
    /* 0.0 */ 0.0000, /* 0.5 */ 1.1180, /* 1.0 */ 1.5811, /* 1.5 */ 1.9365,
    /* 2.0 */ 2.2361, /* 2.5 */ 2.5000, /* 3.0 */ 2.7386, /* 3.5 */ 2.9580 };
static const double s_table_prod_with_3_0[] = { 
    /* 0.0 */ 0.0000, /* 0.5 */ 1.2247, /* 1.0 */ 1.7321, /* 1.5 */ 2.1213,
    /* 2.0 */ 2.4495, /* 2.5 */ 2.7386, /* 3.0 */ 3.0000, /* 3.5 */ 3.2404 };
static const double s_table_prod_with_3_5[] = { 
    /* 0.0 */ 0.0000, /* 0.5 */ 1.3229, /* 1.0 */ 1.8708, /* 1.5 */ 2.2913,
    /* 2.0 */ 2.6458, /* 2.5 */ 2.9580, /* 3.0 */ 3.2404, /* 3.5 */ 3.5000 };

static const double * s_prod_tables[] = {
    s_table_prod_with_0_0, s_table_prod_with_0_5, s_table_sqrt,          s_table_prod_with_1_5,
    s_table_prod_with_2_0, s_table_prod_with_2_5, s_table_prod_with_3_0, s_table_prod_with_3_5
};

double lookup_sqrt_prod(double n1, double n2)
{
    int prod_tab_idx = get_num_idx(n1);
    const double * prod_table = s_prod_tables[prod_tab_idx];
    int prod_fak_idx = get_num_idx(n2);
    return prod_table[prod_fak_idx];
}
