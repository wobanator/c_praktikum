#pragma once

extern double lookup_sqrt(double num);
extern double lookup_sqrt_prod(double n1, double n2);
