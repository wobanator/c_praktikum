#include <stdio.h>
#include <float.h>

int main(void)
{
	long double value = 1.123456789;
	printf("print as %%g: %g\n", value);
	printf("print as %%llg: %llg\n", value);

	return 0;
}


