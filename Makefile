
SOURCES = $(wildcard *.c)
HEADER  = $(wildcard *.h)
OBJECTS = $(SOURCES:%.c=%.o)
TARGET  = vektor_test
CFLAGS  = -O2 -Wall -W 
CC      = gcc

# link target (depends on .o-files)
$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) -lm -o $(TARGET)

# objects depend on all input files 
$(OBJECTS): $(HEADER) $(SOURCES)

# create .o-files out of .c-files
.c.o:
	$(CC) -c $(CFLAGS) $< -o $@

# PHONY-targets are not depend on file with the same name
.PHONY: clean
clean:
	rm -f $(OBJECTS)
	rm -f $(TARGET)
